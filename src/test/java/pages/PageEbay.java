package pages;

import java.io.IOException;
import java.util.Properties;
import java.util.Vector;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.webdriver.WebDriverFacade;

public class PageEbay extends PageObject {

	String listaNomComple, listaPrecComple;
	private static final Properties properties = new Properties();

	{

		java.io.InputStream resource = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("configuracion.properties");
		try {
			properties.load(resource);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FindBy(id = "gh-ac")
	private WebElementFacade cajaBuscar;

	@FindBy(id = "gh-btn")
	private WebElementFacade botonBuscar;

	@FindBy(id = "x-refine__group_1__0")
	private WebElementFacade bloqueTalla;

	@FindBy(id = "srp-river-results")
	private WebElementFacade ListaProductos;

	@FindBy(xpath = "/html/body/div[3]/div[5]/div[1]/div/div[1]/div[3]/div[1]/div/button")
	private WebElementFacade ordenarPor;

	@FindBy(xpath = "/html/body/div[3]/div[5]/div[1]/div/div[1]/div[3]/div[1]/div/div/div/ul")
	private WebElementFacade listaComboOrden;

	@FindBy(xpath = "/html/body/div[3]/div[4]/ul/li[1]/ul/li[2]/ul/li[1]/div[2]/div[1]/div")
	private WebElementFacade cajaMarca;

	@FindBy(className = "x-searchable-list__textbox__aspect-Brand")
	private WebElementFacade cajaMarcax;

	@FindBy(id = "srp-river-results")
	private WebElementFacade ListaProductoFiltrados;

	public List<String> ListaFiltroProductosPrecio() {
		List<String> currentOptions = new ArrayList<String>();
		WebElement ul = ListaProductoFiltrados.findElement(By.xpath("ul"));
		List<WebElement> lista = ul.findElements(By.xpath("li/div/div[2]/div[3]/div[1]/span"));
		System.out.println("LISTA DESCENDENTE DE PRECIOS DE LOS 5 PRODUCTOS");
		for (int i = 0; i < lista.size(); i++) {
			currentOptions.add(lista.get(0).getText());
			currentOptions.add(lista.get(1).getText());
			currentOptions.add(lista.get(2).getText());
			currentOptions.add(lista.get(3).getText());
			currentOptions.add(lista.get(4).getText());
			Collections.reverse(currentOptions);
			System.out.println(currentOptions+"\r\n");
			break;

		}
		return currentOptions;

	}

	public void productoConPrecio() {

		WebElement ulCompleta = ListaProductoFiltrados.findElement(By.xpath("ul"));
		List<WebElement> listaNombreCompleta = ulCompleta.findElements(By.xpath("li/div/div[2]/a/h3"));
		List<WebElement> listaPrecioCompleta = ulCompleta.findElements(By.xpath("li/div/div[2]/div[3]/div[1]/span"));
		for (int i = 0; i < listaNombreCompleta.size() && i < listaPrecioCompleta.size(); i++) {
			listaNomComple = listaNombreCompleta.get(i).getText();
			listaPrecComple = listaPrecioCompleta.get(i).getText();
			System.out.println(listaNomComple + " tiene el precio de: " + listaPrecComple);		
		}

	}

	public Vector<String> ListaFiltroProductosNombre() {
		Vector<String> miVector = new Vector<String>();
		WebElement ulNombre = ListaProductoFiltrados.findElement(By.xpath("ul"));
		List<WebElement> listaNombre = ulNombre.findElements(By.xpath("li/div/div[2]/a/h3"));
		System.out.println("LISTA ASCENDENTE DE NOMBRES DE LOS 5 PRODUCTOS");
		for (int i = 0; i < listaNombre.size(); i++) {

			miVector.add(listaNombre.get(0).getText());
			miVector.add(listaNombre.get(1).getText());
			miVector.add(listaNombre.get(2).getText());
			miVector.add(listaNombre.get(3).getText());
			miVector.add(listaNombre.get(4).getText());
			Collections.sort(miVector);
			System.out.println(miVector+"\r\n");
			break;
		}
		return miVector;
	}

	public void getUrl() {
		getAllWebDriver().get(properties.getProperty("ebay.url"));
		getAllWebDriver().manage().window().maximize();
	}

	public void BuscarZapatos(String producto) {

		WebDriverWait wait2 = new WebDriverWait(getAllWebDriver(), 20);
		wait2.until(ExpectedConditions.visibilityOf(cajaBuscar));
		cajaBuscar.sendKeys(producto);
		botonBuscar.click();

	}

	public void SeleccionMarca(String marca) {
		WebDriverWait wait2 = new WebDriverWait(getAllWebDriver(), 20);
		wait2.until(ExpectedConditions.visibilityOf(cajaMarcax));
		cajaMarcax.sendKeys(marca);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cajaMarcax.sendKeys(Keys.TAB, Keys.ENTER);

	}

	public void SeleccionTalla(String talla) {
//		WebDriverWait wait2 = new WebDriverWait(getAllWebDriver(), 20);
//		wait2.until(ExpectedConditions.visibilityOf(bloqueTalla));
		WebElement ul = bloqueTalla.findElement(By.tagName("ul"));
		List<WebElement> lista = ul.findElements(By.xpath("li/div/a/div/div/span"));
		for (WebElement elementos : lista) {
			if (elementos.getText().equals(talla)) {
				System.out.println("Talla seleccionada: " + elementos.getText());
				elementos.click();
				break;
			}

		}

	}

	public void ContarResultado() {
		WebDriverWait wait2 = new WebDriverWait(getAllWebDriver(), 20);
		wait2.until(ExpectedConditions.visibilityOf(ListaProductos));
		WebElement ul = ListaProductos.findElement(By.xpath("ul"));
		List<WebElement> li = ul.findElements(By.xpath("li"));
		int valor = li.size();
		System.out.println("Cantidad de productos: " + valor);
	}

	public void OrdenAscendente(String orden) {
		ordenarPor.click();
		ordenarPor.click();
		WebDriverWait wait2 = new WebDriverWait(getAllWebDriver(), 60);
		wait2.until(ExpectedConditions.visibilityOf(ordenarPor));
		List<WebElement> listas = listaComboOrden.findElements(By.xpath("li/a/span"));
		System.out.println(listas.size());
		for (WebElement elementoOrden : listas) {
			if (elementoOrden.getText().equals(orden)) {
				System.out.println("Talla seleccionada: " + elementoOrden.getText());
				elementoOrden.click();
				break;
			}
		}

	}

	private WebDriver getAllWebDriver() {
		WebDriverFacade facade = (WebDriverFacade) getDriver();
		return facade.getProxiedDriver();
	}

}