package features.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.steps.serenity.StepCompraZapatos;
import net.thucydides.core.annotations.Steps;

public class CucumberCompraZapatos {

	@Steps
	StepCompraZapatos steCompra;

	@Given("^buscamos el (.*) por (.*) y (.*)$")
	public void buscamos_el_producto_por_talla(String producto,String talla,String marca) {
		steCompra.getUrl();
		steCompra.buscarZapato(producto);
		steCompra.SeleccionTalla(talla);
		steCompra.SeleccionMarca(marca);
		
		
	}
	
	@And("^ordenamos por precio (.*) el producto$")
	public void ordenamos_por_precio_ascendente_el_producto(String orden) {
		steCompra.ContarResultado();
		steCompra.OrdenAscendente(orden);
 	}
	
	
	@When("^imprimimos en consola los primeros 5 productos con sus precios$")
	public void imprimimos_en_consola_los_productos_con_sus_precios() {
		steCompra.productoConPrecio();
 	}	
	
	@Then("^ordenamos e imprimimos los productos por nombre ascendente$")
	public void ordenamos_e_imprimimos_los_productos_por_nombres_ascendentes() {
		steCompra.ListaFiltroProductosNombre();
 	}
	
	@And("^ordenamos e imprimimos los precios en modo descendente$")
	public void ordenamos_e_imprimimos_los_precios_en_modo_descendentes() {
		steCompra.ListaFiltroProductosPrecio();
	}	

}