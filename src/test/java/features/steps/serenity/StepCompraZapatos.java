package features.steps.serenity;


import net.thucydides.core.annotations.Step;
import pages.PageEbay;


public class StepCompraZapatos {

	PageEbay page;
	

	@Step
	public void getUrl() {
		page.getUrl();
	}
	
	@Step
	public void buscarZapato(String producto) {
		page.BuscarZapatos(producto);
	}
	
	@Step
	public void SeleccionTalla(String talla) {
		page.SeleccionTalla(talla);
	}
	
	@Step
	public void ContarResultado() {
		page.ContarResultado();
	}
	@Step
	public void OrdenAscendente(String orden) {
		page.OrdenAscendente(orden);
	}

	@Step
	public void SeleccionMarca(String marca) {
		page.SeleccionMarca(marca);
	}
	@Step
	public void ListaFiltroProductosPrecio() {
		page.ListaFiltroProductosPrecio();
	}
	@Step
	public void ListaFiltroProductosNombre() {
		page.ListaFiltroProductosNombre();
	}
	@Step
	public void productoConPrecio() {
		page.productoConPrecio();
	}
	
}
