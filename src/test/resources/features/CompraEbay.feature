Feature: Funcionalidad de Crear Cotizacion Completa de un Seguro Vehicular

@pruebaEbay
Scenario Outline: Realizar pedido de zapatos en Ebay
	Given buscamos el <producto> por <talla> y <marca>
	And ordenamos por precio <orden> el producto
	When imprimimos en consola los primeros 5 productos con sus precios
	Then ordenamos e imprimimos los productos por nombre ascendente
	And ordenamos e imprimimos los precios en modo descendente
	
	Examples: 
		|producto|talla|marca|orden|
		|zapatos|10|puma|Precio + Envío: más bajo primero|